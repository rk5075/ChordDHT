import java.rmi.*;
import java.rmi.server.*;
import java.util.*;
import java.io.*;

public class registrationServer extends UnicastRemoteObject implements registrationServerInterface{

    private ArrayList<Integer> onlineNodes = new ArrayList(16);
    private ArrayList<Integer> successorNodes = new ArrayList(4);
    private ArrayList<Integer> desiredNodes = new ArrayList(4);
    private ArrayList<Node> nodeList = new ArrayList(16);
    private HashMap<String, Integer> hmap = new HashMap<String, Integer>();
    private HashMap<String, ArrayList<String>> nodeAndFile = new HashMap<String, ArrayList<String>>();
    private ArrayList<String> temp = new ArrayList<String>();
    private int nodeNumber;

    registrationServer() throws RemoteException {
        super();
    }

    public static int hashCode(String s)
    {
        int hash = 7;
        for (int i = 0; i < s.length(); i++)
        {
            hash = hash*31 + s.charAt(i);
        }
        return Math.abs(hash%16);
    }

    public int add(int x, int y) throws RemoteException {
        return x+y;
    }

    public void nodeGoesOffline(Node node)
    {
        int nodeNumber= hmap.get(node.getNodeId());
        onlineNodes.remove(nodeNumber);
        nodeList.remove(node);
        hmap.remove(node.getNodeId());
        System.out.println("nodes left online "+ onlineNodes);
    }

    public ArrayList<Integer> requestOnlineNode() throws RemoteException {
        return onlineNodes;
    }

    public ArrayList<String> pushFile(String filename, Node node) throws RemoteException {
        int nodeValueOfFile = hashCode(filename);
        int flag = 0;
        if (onlineNodes.contains(nodeValueOfFile))
        {
            for (Node n : nodeList)
                if(n.getNodeId().equalsIgnoreCase(node.getNodeId()))
                {
                    ArrayList<String> tempList = nodeAndFile.get(n.getNodeId());
                    tempList = new ArrayList<String>();
                    tempList.add(filename);
                    temp.add(filename);
                    nodeAndFile.put(n.getNodeId(), tempList);
                    n.setFiles(temp);
                    node.setFiles(temp);
                }
            return node.getFiles();
        }
        else
        {
            for (int nextNode : onlineNodes)
            {
                if (nextNode>=nodeValueOfFile && flag == 0)
                {
                    Integer intValue=nextNode;
                    String key = null;
                    for(Map.Entry entry: hmap.entrySet()){
                        if(intValue.equals(entry.getValue())){
                            key = (String)entry.getKey();
                            flag =1;
                            break; //breaking because its one to one map
                        }
                    }
                    for (Node n : nodeList)
                    {
                        if (n.getNodeId().equalsIgnoreCase(key))
                        {
                            ArrayList<String> tempList = nodeAndFile.get(n.getNodeId());
                            tempList = new ArrayList<String>();
                            tempList.add(filename);
                            temp.add(filename);
                            nodeAndFile.put(n.getNodeId(), tempList);
                            System.out.println(tempList);
                            n.setFiles(temp);
                            node.setFiles(temp);
                        }
                    }
                }
                if (nextNode < nodeValueOfFile && flag ==0)
                {
                    Integer intValue=nextNode;
                    String key = null;
                    for(Map.Entry entry: hmap.entrySet()){
                        if(intValue.equals(entry.getValue())){
                            key = (String)entry.getKey();
                            flag = 1;
                            break; //breaking because its one to one map
                        }
                    }
                    for (Node n : nodeList)
                    {
                        if (n.getNodeId().equalsIgnoreCase(key))
                        {
                            ArrayList<String> tempList = nodeAndFile.get(n.getNodeId());
                            tempList = new ArrayList<String>();
                            tempList.add(filename);
                            temp.add(filename);
                            nodeAndFile.put(n.getNodeId(), tempList);
                            System.out.println(tempList);
                            n.setFiles(temp);
                            node.setFiles(temp);
                        }
                    }
                }
            }
            return node.getFiles();
        }

    }

    public int joinNetwork(String ipAddress, Node node) throws RemoteException {
        System.out.println("The node "+node.getNodeId()+" joined with ip "+ipAddress);
        node.setNodeNumber(hashCode(ipAddress));
        nodeList.add(node);
        hmap.put(node.getNodeId(), node.getNodeNumber());
        for (int i_iter = 0; i_iter < onlineNodes.size(); i_iter++)
            if (onlineNodes.get(i_iter) == node.getNodeNumber())
            {
                int temp = node.getNodeNumber() + 1;
                node.setNodeNumber(temp);
                if(node.getNodeNumber()==16)
                    node.setNodeNumber(0);
            }
        System.out.println("nodeNumber assigned "+ node.getNodeNumber());
        onlineNodes.add(node.getNodeNumber());
        Collections.sort(onlineNodes);
        return node.getNodeNumber();
    }

    public static void main(String args[])
    {
        try{
            registrationServerInterface stub = new registrationServer();
            Naming.rebind ("localhost", stub);
            System.out.println("Server has started");
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }
}