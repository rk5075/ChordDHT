import java.rmi.*;
import java.util.*;

public interface registrationServerInterface extends Remote{
    public int add(int x, int y) throws RemoteException;
    public ArrayList<Integer> requestOnlineNode() throws RemoteException;
    public ArrayList<String> pushFile(String filename, Node node) throws RemoteException;
    public int joinNetwork(String ipAddress, Node node) throws RemoteException;
    public void nodeGoesOffline(Node node) throws RemoteException;
}