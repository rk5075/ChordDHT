import java.net.*;
import java.rmi.*;
import java.net.*;
import java.util.*;
import java.io.*;
import java.io.Serializable;

class Node implements Serializable{
    private static final long serialVersionUID = 1545135876274538840L;

    private String nodeId;
    private int nodeNumber;
    private ArrayList<String> files;
    private ArrayList<Integer> desiredNodes;
    private ArrayList<Integer> successorNodes;

    public Node(String nodeId, ArrayList<String> files, ArrayList<Integer> desiredNodes, ArrayList<Integer> successorNodes) {
        this.nodeId = nodeId;
        this.files = files;
        this.desiredNodes = desiredNodes;
        this.successorNodes = successorNodes;
    }

    public Node() {

    }

    public String getNodeId()
    {
        return nodeId;
    }

    public void setNodeId(String nodeId){
        this.nodeId = nodeId;
    }

    public int getNodeNumber() {
        return nodeNumber;
    }

    public void setNodeNumber(int nodeNumber) {
        this.nodeNumber = nodeNumber;
    }

    public ArrayList<Integer> getSuccessorNodes() {
        return successorNodes;
    }

    public void setSuccessorNodes(ArrayList<Integer> successorNodes) {
        this.successorNodes = successorNodes;
    }

    public ArrayList<Integer> getDesiredNodes() {
        return desiredNodes;
    }

    public void setDesiredNodes(ArrayList<Integer> desiredNodes) {
        this.desiredNodes = desiredNodes;
    }

    public ArrayList<String> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<String> files) {
        this.files = files;
    }
}


public class nodeSpace{

    public static void displayFingerTable(ArrayList<Integer> onlineNodes, Node n)
    {
        ArrayList<Integer> desiredNodes = new ArrayList<Integer>(4);
        ArrayList<Integer> successorNodes = new ArrayList<Integer>(4);
        for (int i =0; i < 4; i++)
        {
            int linkNode = n.getNodeNumber()+(int)Math.pow(2,i);
            desiredNodes.add(linkNode);
        }
        for (int i=0; i < 4 ; i++)
        {
            if (onlineNodes.contains(desiredNodes.get(i)))
            {
                successorNodes.add(desiredNodes.get(i));
            }
            else
            {
                int hav = 0;
                for (int temp =0; temp < onlineNodes.size(); temp++)
                    if (onlineNodes.get(temp)>desiredNodes.get(temp))
                        hav = onlineNodes.get(temp);
                if (hav == 0)
                    hav = onlineNodes.get(hav);
                successorNodes.add(hav);
            }
        }
        n.setDesiredNodes(desiredNodes);
        n.setSuccessorNodes(successorNodes);
        System.out.println("Finger table for node "+ n.getNodeId()+"is ");
        System.out.println("Desired Nodes "+ desiredNodes);
        System.out.println("Successor Nodes "+ successorNodes);
    }

    public static int hashCode(String s)
    {
        int hash = 7;
        for (int i = 0; i < s.length(); i++)
        {
            hash = hash*31 + s.charAt(i);
        }
        return hash%16;
    }

    public static void main(String args[])
    {
        try{
            Scanner sc = new Scanner(System.in);
            registrationServerInterface stub = (registrationServerInterface)Naming.lookup("localhost");
            System.out.println("Enter the name of node");
            String nodeName = sc.next();
            InetAddress IP=InetAddress.getLocalHost();
            Node n = new Node();
            n.setNodeId(nodeName);
            n.setNodeNumber(stub.joinNetwork(IP.getHostAddress(), n));
            //System.out.println(stub.add(3,4));
            boolean done = false;
            while(!done)
            {
                int ch;
                System.out.println("Select an option");
                System.out.println("1. Display Finger Table");
                System.out.println("2. Push a file");
                System.out.println("3. Exit");
                ch = sc.nextInt();
                switch(ch)
                {
                    case 1:
                        ArrayList<Integer> onlineNodes = stub.requestOnlineNode();
                        displayFingerTable(onlineNodes, n);
                        break;
                    case 2:
                        System.out.println("enter the name of file");
                        String filename = sc.next();
                        ArrayList<String> tempFilesList = stub.pushFile(filename,n);
                        n.setFiles(tempFilesList);
                        System.out.println("The filelist for this node is "+ n.getFiles());
                        break;
                    case 3:
                        stub.nodeGoesOffline(n);
                        done = true;
                        break;
                }

            }
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }
}